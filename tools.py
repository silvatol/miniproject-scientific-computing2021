import matplotlib.pylab as plt
import netCDF4
import numpy as np
import netCDF4 as nc

def mod_arr(field,axis,order):
    """Define array with boundary conditions to compute partial derivatives for first order forward scheme. 
    Boundary condition for last row or column:N=N+(N-(N-1)) of field array
    • Field: field to derive (array to modify)
    • axis = 1, colums varies (used to compute derivates along x). axis = 0 rows varies (used to compute derivates along y) 
    • order=order of precision of derivatives"""
    if order==1:
        mod_arr=np.roll(field,-1,axis=axis)
        if axis==0:
            mod_arr[-1,:]=2*field[-1,:]-field[-2,:]
        elif axis==1: 
            mod_arr[:,-1]=2*field[:,-1]-field[:,-2]
    return mod_arr


def derivatives(field1,field2,axis,order):
    """Compute partial derivatives along a field called field2 which could be in time or space using first-order forward scheme
    • Field1: Field to derive
    • Field2: Field to derive from
    • axis: along which derivation is carried out.
    axis=1 derivation along i direction, axis=0 derivation along j direction
    • order= order of precision of derivatives, this function can be expanded to a higher order of precision"""
    if order==1:
        der=np.divide((mod_arr(field1,axis,order)-field1),(mod_arr(field2,axis,order)-field2))
    return der

